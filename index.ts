import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordCloudComponent } from './src/word-cloud.component';

export * from './src/word-cloud.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    WordCloudComponent,
  ],
  exports: [
    WordCloudComponent,
  ]
})
export class WordCloudModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: WordCloudModule,
      providers: []
    };
  }
}
